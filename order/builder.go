package order

import (
	"errors"
	"fmt"

	"github.com/alpacahq/alpaca-trade-api-go/alpaca"
	"github.com/shopspring/decimal"
)

// Builder simplifies creation of alpaca.PlaceOrderRequest
type Builder struct {
	supportedTIF map[string]bool

	side        alpaca.Side
	symbol      string
	qty         int
	limitPrice  float64
	stopPrice   float64
	timeInForce string
}

// NewBuilder creates a new *Builder
func NewBuilder() *Builder {
	return &Builder{
		supportedTIF: map[string]bool{
			"day": true,
			"gtc": true,
			"opg": true,
			"cls": true,
			"ioc": true,
			"fok": true,
		},
		timeInForce: "day",
		qty:         1,
	}
}

// Side sets the side for the order
func (b *Builder) Side(side alpaca.Side) *Builder {
	b.side = side
	return b
}

// Symbol sets the symbol for the order
func (b *Builder) Symbol(symbol string) *Builder {
	b.symbol = symbol
	return b
}

// Quantity sets the number of stocks for the order
func (b *Builder) Quantity(qty int) *Builder {
	b.qty = qty
	return b
}

// LimitPrice sets the limit price for the order
func (b *Builder) LimitPrice(limitPrice float64) *Builder {
	b.limitPrice = limitPrice
	return b
}

// StopPrice sets the stop price for the order
func (b *Builder) StopPrice(stopPrice float64) *Builder {
	b.stopPrice = stopPrice
	return b
}

// TimeInForce sets the time in force for the order
func (b *Builder) TimeInForce(tif string) *Builder {
	b.timeInForce = tif
	return b
}

// Build creates the order request and validates the data
func (b *Builder) Build() (alpaca.PlaceOrderRequest, error) {
	req := alpaca.PlaceOrderRequest{}

	if b.qty < 1 {
		return req, errors.New("Quantity canot be less than 1")
	}

	if b.limitPrice < 0 {
		return req, errors.New("Limit price cannot be negative")
	}

	if b.stopPrice < 0 {
		return req, errors.New("Stop price cannot be negative")
	}

	if !b.supportedTIF[b.timeInForce] {
		return req, fmt.Errorf("Time in force %s is not supported", b.timeInForce)
	}

	req = alpaca.PlaceOrderRequest{
		AssetKey:    &b.symbol,
		Qty:         decimal.NewFromFloat(float64(b.qty)),
		Side:        b.side,
		TimeInForce: alpaca.TimeInForce(b.timeInForce),
	}

	if b.limitPrice > 0 && b.stopPrice > 0 {
		req.Type = alpaca.StopLimit
		lim := decimal.NewFromFloat(b.limitPrice)
		stop := decimal.NewFromFloat(b.stopPrice)
		req.StopLoss = &alpaca.StopLoss{
			LimitPrice: &lim,
			StopPrice:  &stop,
		}
	} else if b.limitPrice > 0 {
		req.Type = alpaca.Limit
		lim := decimal.NewFromFloat(b.limitPrice)
		req.LimitPrice = &lim
	} else if b.stopPrice > 0 {
		req.Type = alpaca.Stop
		stop := decimal.NewFromFloat(b.stopPrice)
		req.StopPrice = &stop
	} else {
		req.Type = alpaca.Market
	}

	return req, nil
}
