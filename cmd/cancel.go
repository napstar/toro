package cmd

import (
	"errors"
	"fmt"

	"github.com/spf13/cobra"
)

type canceller interface {
	CancelAllOrders() error
	CancelOrder(orderID string) error
}

type cancelCmd struct {
	cmd *cobra.Command

	all bool
}

func newCancelCmd() *cancelCmd {
	c := &cancelCmd{
		cmd: &cobra.Command{
			Use:   "cancel ORDER_ID",
			Short: "Cancel one or more open orders",
		},
	}

	c.cmd.RunE = func(cmd *cobra.Command, args []string) error {
		if len(args) < 1 && !c.all {
			return errors.New("Must supply order ids or \"all\" flag to cancel any orders")
		}

		client, err := getAlpacaClient()

		if err != nil {
			return err
		}

		return c.Cancel(args, client)
	}

	c.cmd.Flags().BoolVarP(&c.all, "all", "a", false, "cancel all open orders")

	return c
}

func (c *cancelCmd) GetCommand() *cobra.Command {
	return c.cmd
}

func (c *cancelCmd) Cancel(ids []string, canceller canceller) error {
	if c.all {
		err := canceller.CancelAllOrders()

		if err != nil {
			return err
		}

		fmt.Println("✅ Orders cancelled")

		return nil
	}

	for _, id := range ids {
		err := canceller.CancelOrder(id)

		if err != nil {
			return err
		}

		fmt.Printf("✅ Order cancelled: %s\n", id)
	}

	return nil
}
