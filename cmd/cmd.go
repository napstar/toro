package cmd

import (
	"errors"
	"fmt"
	"io"
	"os"
	"text/tabwriter"

	"github.com/alpacahq/alpaca-trade-api-go/alpaca"
	"github.com/alpacahq/alpaca-trade-api-go/common"
	"github.com/mitchellh/go-homedir"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var cfgFile string

func getAlpacaClient() (*alpaca.Client, error) {
	mode := viper.GetString("mode")

	if mode == "" {
		fmt.Println("Mode not specified. Defaulting to \"paper\". To trade with real money, set mode to \"live\"")
		mode = "paper"
	} else if !(mode == "paper" || mode == "live") {
		return nil, fmt.Errorf("Mode \"%s\" is invalid", mode)
	}

	if mode == "paper" {
		alpaca.SetBaseUrl("https://paper-api.alpaca.markets")
	}

	id := viper.GetString("id")

	if id == "" {
		return nil, errors.New("id not set")
	}

	secret := viper.GetString("secret")

	if secret == "" {
		return nil, errors.New("secret not set")
	}

	return alpaca.NewClient(&common.APIKey{
		ID:     id,
		Secret: secret,
	}), nil
}

func getTabWriter() *tabwriter.Writer {
	return tabwriter.NewWriter(os.Stdout, 0, 4, 2, ' ', tabwriter.AlignRight)
}

type writeFlusher interface {
	io.Writer

	Flush() error
}

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "toro",
	Short: "toro is a CLI tool for trading stocks",
	Long: `toro is a tool for stock trading with your personal Alpaca account.
	`,
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	cobra.OnInitialize(initConfig)
	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.toro.yaml)")

	rootCmd.AddCommand(newConfigCmd().GetCommand())
	rootCmd.AddCommand(newGetCmd().GetCommand())
	rootCmd.AddCommand(newBuyCmd().GetCommand())
	rootCmd.AddCommand(newSellCmd().GetCommand())
	rootCmd.AddCommand(newOrdersCmd().GetCommand())
	rootCmd.AddCommand(newCancelCmd().GetCommand())
	rootCmd.AddCommand(newReportCmd().GetCommand())
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func initConfig() {
	cfgFlag := cfgFile != ""
	if cfgFlag {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := homedir.Dir()
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		viper.AddConfigPath(home)
		viper.SetConfigName(".toro")
		cfgFile = home + "/.toro.yaml"
	}

	viper.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			// ignore if file not found, unless it is specified
			if cfgFlag {
				fmt.Println(err)
				os.Exit(1)
			}
		} else {
			fmt.Println(err)
			os.Exit(1)
		}
	}
}
